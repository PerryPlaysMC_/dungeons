package me.perryplaysmc.dungeon;

import me.perryplaysmc.dungeon.commands.CommandDungeon;
import me.perryplaysmc.dungeon.dungeon.Dungeon;
import me.perryplaysmc.dungeon.dungeon.DungeonManager;
import me.perryplaysmc.dungeon.util.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/6/19-2023
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 *
 **/

@SuppressWarnings("all")
public class DungeonMain implements Listener {
    
    private static DungeonMain instance;
    public Scoreboard scoreboard = Bukkit.getServer().getScoreboardManager().getMainScoreboard();
    private Config config;
    CommandDungeon d;
    ConsoleCommandSender s = Bukkit.getServer().getConsoleSender();
    
    public void onEnable() {
        instance = this;
        config = new Config("plugins/BendingUnbroken", "config.yml");
        s.sendMessage("§cBendersUnbroken Dungeons>>§a Enabled");
        if(scoreboard.getTeam("shulker")!=null)
            scoreboard.getTeam("shulker").unregister();
        scoreboard.registerNewTeam("shulker");//You need to create a different team for every color
        Team t = scoreboard.getTeam("shulker");
        t.setPrefix(ChatColor.GREEN + ""); //Here you can set the color using ChatColor or using  minecraft color codes with §
        t.setColor(ChatColor.GREEN);
        t.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
        
        Bukkit.getPluginManager().registerEvents(this, Load.getPlugin(Load.class));
        d = new CommandDungeon();
        Bukkit.getPluginManager().registerEvents(d, Load.getPlugin(Load.class));
        Bukkit.getServer().getPluginCommand("dungeon").setExecutor(d);
        Bukkit.getServer().getPluginCommand("dungeon").setTabCompleter(d);
        File dir = new File("plugins/BendingUnbroken/Dungeons");
        if(dir.isDirectory()&&dir.listFiles()!=null&&dir.listFiles().length>0)
            for(File f : dir.listFiles()) {
                if(!f.getName().endsWith(".yml"))continue;
                Config cfg = new Config(dir, f, YamlConfiguration.loadConfiguration(f));
                new Dungeon(cfg);
            }
    }
    
    public void onDisable() {
        s.sendMessage("§cBendersUnbroken Dungeons>>§4 Disabled");
        for(Entity e : d.entities)
            e.remove();
        d.entities.clear();
    }
    
    
    @EventHandler
    void onExplode(EntityExplodeEvent e) {
        if(e.getEntity() instanceof Creeper) {
            Creeper c = (Creeper) e.getEntity();
            if(c.getCustomName().equalsIgnoreCase("§cCreeper")) {
                e.setCancelled(true);
                double maxHealth = c.getMaxHealth();
                double health = c.getHealth();
                c.setMaxHealth(20*10);
                c.setHealth(c.getMaxHealth());
                c.getWorld().createExplosion(
                        c.getLocation().getX(),
                        c.getLocation().getY(),
                        c.getLocation().getZ(), 1, false, false);
                c.setMaxHealth(maxHealth);
                c.setHealth(health);
            }
        }
    }
    
    HashMap<String, Dungeon> users = new HashMap<>();
    
    @EventHandler
    void onMove(PlayerMoveEvent e) {
        if(users.containsKey(e.getPlayer().getName())) {
            Dungeon d = users.get(e.getPlayer().getName());
            if(!d.isWithinBoarder(e.getTo())) {
                if(config.getBoolean("settings.exit-enter"))
                    e.getPlayer().sendTitle("", ChatColor.translateAlternateColorCodes('&',
                            config.getString("settings.ExitMessage").replace("{0}", d.getName())), 20, 40, 20);
                d.getPlayers().remove(e.getPlayer());
                users.remove(e.getPlayer().getName());
            }
            return;
        }
        for(Dungeon d : DungeonManager.getDungeons())
            if(d!=null) {
                if(d.isWithinBoarder(e.getTo())) {
                    if(!users.containsKey(e.getPlayer().getName())) {
                        if(config.getBoolean("settings.exit-enter"))
                            e.getPlayer().sendTitle("",
                                    ChatColor.translateAlternateColorCodes('&',
                                            config.getString("settings.EnterMessage").replace("{0}", d.getName())), 20, 40, 20);
                        d.getPlayers().add(e.getPlayer());
                        users.put(e.getPlayer().getName(), d);
                        break;
                    }
                } else {
                    if(users.containsKey(e.getPlayer().getName())) {
                        if(config.getBoolean("settings.exit-enter"))
                            e.getPlayer().sendTitle("", ChatColor.translateAlternateColorCodes('&',
                                    config.getString("settings.ExitMessage").replace("{0}", d.getName())), 20, 40, 20);
                        d.getPlayers().remove(e.getPlayer());
                        users.remove(e.getPlayer().getName());
                        break;
                    }
                }
            }
    }
    
    public InputStream getResource(String file) {
        return Load.getPlugin(Load.class).getResource(file);
    }
    
    public Config getConfig() {
        return config;
    }
    
    public static DungeonMain getInstance() {
        return instance;
    }
    
}
