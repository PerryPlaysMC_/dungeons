package me.perryplaysmc.dungeon;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/6/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Load extends JavaPlugin {
    
    DungeonMain main;
    private static Load inst;
    
    @Override
    public void onEnable() {
        inst = this;
        main = new DungeonMain();
        main.onEnable();
    }
    
    @Override
    public void onDisable() {
        main.onDisable();
    }
    
    public static Load getInstance() {
        return inst;
    }
    
}
