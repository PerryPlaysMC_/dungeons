package me.perryplaysmc.dungeon.commands;

import me.perryplaysmc.dungeon.DungeonMain;
import me.perryplaysmc.dungeon.Load;
import me.perryplaysmc.dungeon.dungeon.Dungeon;
import me.perryplaysmc.dungeon.dungeon.DungeonManager;
import me.perryplaysmc.dungeon.dungeon.MobType;
import me.perryplaysmc.dungeon.dungeon.Spawner;
import me.perryplaysmc.dungeon.util.StringUtils;
import net.minecraft.server.v1_14_R1.PacketPlayOutWorldParticles;
import net.minecraft.server.v1_14_R1.ParticleParam;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.lang.reflect.Constructor;
import java.util.*;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/6/19-2023
 * <p>
 *
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandDungeon implements CommandExecutor, TabCompleter, Listener {
    
    private HashMap<UUID, Location> left = new HashMap<>(), right = new HashMap<>();
    private HashMap<Player, BukkitTask> tasks = new HashMap<>(), tasks2 = new HashMap<>();
    private HashMap<CommandSender, Dungeon> current = new HashMap<>(), selected = new HashMap<>();
    private List<String> right1 = new ArrayList<>(), left1 = new ArrayList<>();
    public List<Entity> entities = new ArrayList<>();
    private
    List<String> users = new ArrayList<>();
    private ItemStack item;
    private int i = 0;
    
    public CommandDungeon() {
        item = new ItemStack(Material.STICK);
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(StringUtils.translate(
                DungeonMain.getInstance().getConfig().getString("Wand.name")
        ));
        im.setLore(StringUtils.translateList(
                DungeonMain.getInstance().getConfig().getStringList("Wand.lore"))
        );
        item.setItemMeta(im);
    }
    
    void sendMessage(CommandSender s, String path, Object... objects) {
        if(!DungeonMain.getInstance().getConfig().isSet("Messages." + path)) {
            s.sendMessage("§d§lconfig.yml§7: Invalid location\n§d§lMessages." + path);
            return;
        }
        String msg = DungeonMain.getInstance().getConfig().getString("Messages." + path);
        for(int i = 0; i < objects.length; i++) {
            Object o = objects[i];
            msg = msg.replace("{" + i + "}", o.toString());
        }
        s.sendMessage(StringUtils.translate(msg));
    }
    
    boolean isPlayer(CommandSender s) {
        return s instanceof Player;
    }
    
    @Override
    public boolean onCommand(CommandSender s, Command command, String cl, String[] args) {
        Player p = (Player) s;
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("help")) {
                if(!p.hasPermission("bendingunbroken.dungeon.help")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.help");
                    return true;
                }
                s.sendMessage("§7§l§m<           §r§d§lDungeons Help§r§7§l§m           >");
                s.sendMessage("§7/§ddungeon §8wand §d: §7Give you the wand to view/set boarders");
                s.sendMessage("§7/§ddungeon §8reload §d: §7Reloads the config file");
                s.sendMessage("§7/§ddungeon §8reloadDungeon §d: §7Reloads the dungeon");
                s.sendMessage("§7/§ddungeon §8create <name> §d: §7Creates a dungeon");
                s.sendMessage("§7/§ddungeon §8delete <name> §d: §7Deletes a dungeon");
                s.sendMessage("§7/§ddungeon §8addSpawner <Type> <maxSpawns> §d: §7Adds a spawner the the selected dungeon");
                s.sendMessage("§7/§ddungeon §8setWait <time> §d: §7Sets the wait time for the dungeon to start");
                s.sendMessage("§7/§ddungeon §8setDuration <time> §d: §7Sets the duration time for the dungeon to end");
                s.sendMessage("§7§l§m<                                        >");
            }else if(args[0].equalsIgnoreCase("wand")) {
                if(!p.hasPermission("bendingunbroken.dungeon.wand.give")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.wand.give");
                    return true;
                }
                if(!isPlayer(s)) {
                    sendMessage(s, "Error.mustBePlayer");
                    return true;
                }
                if(p.getInventory().contains(item)) {
                    sendMessage(s, "Error.hasWand");
                    return true;
                }
                p.getInventory().addItem(item);
                sendMessage(s, "givenWand");
                return true;
            }else if(args[0].equalsIgnoreCase("reload")) {
                if(!p.hasPermission("bendingunbroken.dungeon.reloadconfig")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.reloadconfig");
                    return true;
                }
                DungeonMain.getInstance().getConfig().reload();
                sendMessage(s, "reload");
                return true;
            }else if(args[0].equalsIgnoreCase("reloadDungeon")) {
                if(!p.hasPermission("bendingunbroken.dungeon.reloaddungeon")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.reloaddungeon");
                    return true;
                }
                if(!selected.containsKey(s)) {
                    sendMessage(s, "Error.selectDungeon");
                    return true;
                }
                Dungeon d = selected.get(s);
                d.reload();
                sendMessage(s, "reloadDungeon", d.getName());
                return true;
            }else if(args[0].equalsIgnoreCase("redefine")){
                if(!p.hasPermission("bendingunbroken.dungeon.redefine")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.redefine");
                    return true;
                }
                if(!selected.containsKey(s)) {
                    sendMessage(s, "Error.selectDungeon");
                    return true;
                }
                Dungeon d = selected.get(s);
                if(!left.containsKey(p.getUniqueId())) {
                    sendMessage(s, "Error.Set.Pos1");
                    return true;
                }
                if(!right.containsKey(p.getUniqueId())) {
                    sendMessage(s, "Error.Set.Pos2");
                    return true;
                }
                d.redefine(right.get(p.getUniqueId()), left.get(p.getUniqueId()));
                sendMessage(s, "redefine", d.getName(), getLocation(d.getPos1()), getLocation(d.getPos2()));
            }else if(args[0].equalsIgnoreCase("removeSpawner")) {
                if(!p.hasPermission("bendingunbroken.dungeon.removespawner")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.removespawner");
                    return true;
                }
                if(!selected.containsKey(s)) {
                    sendMessage(s, "Error.selectDungeon");
                    return true;
                }
                Dungeon d = selected.get(s);
                Block block = p.getTargetBlock(null, 4);
                if(block == null||!block.getType().isSolid()) {
                    sendMessage(s, "Error.mustLookAtSolidBlock");
                    return true;
                }  
                if(d.getSpawner(block)==null){
                    sendMessage(s, "Error.invalidSpawner", d.getName(), getLocation(block.getLocation()));
                    return true;
                }
                d.removeSpawner(block);
            }else {
                sendMessage(s, "Error.unknownSub");
            }
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("select")) {
                if(!p.hasPermission("bendingunbroken.dungeon.select")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.select");
                    return true;
                }
                Dungeon d = DungeonManager.getDungeon(args[1]);
                if(d==null){
                    sendMessage(s, "Error.invalidDungeon");
                    return true;
                }
                selected.put(s, d);
                sendMessage(s, "selectedDungeon", d.getName());
            }else if(args[0].equalsIgnoreCase("delete")) {
                if(!p.hasPermission("bendingunbroken.dungeon.delete")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.delete");
                    return true;
                }
                Dungeon d = DungeonManager.getDungeon(args[1]);
                if(d==null){
                    sendMessage(s, "Error.invalidDungeon");
                    return true;
                }
                d.delete();
                sendMessage(s, "deleteDungeon", d.getName());
            }else if(args[0].equalsIgnoreCase("setwait")) {
                if(!p.hasPermission("bendingunbroken.dungeon.time.wait")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.time.wait");
                    return true;
                }
                if(!selected.containsKey(s)) {
                    sendMessage(s, "Error.selectDungeon");
                    return true;
                }
                int wait = 0;
                try {
                    wait = Integer.parseInt(args[1].toUpperCase());
                }catch (Exception e) {
                    sendMessage(s, "Error.invalidNumber", args[1]);
                    return true;
                }
                Dungeon d = selected.get(s);
                d.setWaitTime(wait);
                sendMessage(s, "setTime.Wait", d.getName());
            }else if(args[0].equalsIgnoreCase("setduration")) {
                if(!p.hasPermission("bendingunbroken.dungeon.time.duration")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.time.duration");
                    return true;
                }
                if(!selected.containsKey(s)) {
                    sendMessage(s, "Error.selectDungeon");
                    return true;
                }
                int duration = 0;
                try {
                    duration = Integer.parseInt(args[1].toUpperCase());
                }catch (Exception e) {
                    sendMessage(s, "Error.invalidNumber", args[1]);
                    return true;
                }
                Dungeon d = selected.get(s);
                d.setDurationTime(duration);
                sendMessage(s, "setTime.Duration", d.getName());
            }else if(args[0].equalsIgnoreCase("create")) {
                if(!p.hasPermission("bendingunbroken.dungeon.create")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.create");
                    return true;
                }
                if(!left.containsKey(p.getUniqueId())) {
                    sendMessage(s, "Error.Set.Pos1");
                    return true;
                }
                if(!right.containsKey(p.getUniqueId())) {
                    sendMessage(s, "Error.Set.Pos2");
                    return true;
                }
                if(DungeonManager.getDungeon(args[1]) != null) {
                    sendMessage(s, "Error.dungeonExists");
                    return true;
                }
                Dungeon d = new Dungeon(args[1], right.get(p.getUniqueId()), left.get(p.getUniqueId()));
                selected.put(s, d);
                sendMessage(s, "createDungeon", d.getName());
            }else {
                sendMessage(s, "Error.unknownSub");
            }
            return true;
        }
        if(args.length == 3) {
            if(args[0].equalsIgnoreCase("addspawner")) {
                if(!p.hasPermission("bendingunbroken.dungeon.addspawner")&&!p.hasPermission("bendingunbroken.dungeon.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.addspawner");
                    return true;
                }
                if(!selected.containsKey(s)) {
                    sendMessage(s, "Error.selectDungeon");
                    return true;
                }
                Dungeon d = selected.get(s);
                Block block = p.getTargetBlock(null, 4);
                if(block == null||!block.getType().isSolid()) {
                    sendMessage(s, "Error.mustLookAtSolidBlock");
                    return true;
                }
                MobType type;
                try {
                    type = MobType.valueOf(args[1].toUpperCase());
                }catch (Exception e) {
                    sendMessage(s, "Error.invalidMobType");
                    return true;
                }
                int maxSpawns = 0;
                try {
                    maxSpawns = Integer.parseInt(args[2].toUpperCase());
                }catch (Exception e) {
                    sendMessage(s, "Error.invalidNumber", args[2]);
                    return true;
                }
                d.addSpawner(new Spawner(d, block, type, maxSpawns));
                sendMessage(s, "addedSpawner", d.getName(), type.name(), maxSpawns, d.getSpawners().size());
            }
        }
        return true;
    }
    
    @Override
    public List<String> onTabComplete(CommandSender wrgerg, Command command, String cl, String[] args) {
        List<String> f = new ArrayList<>();
        List<String> arg1 = Arrays.asList("wand", "select", "reload", "create", "help");
        if(selected.containsKey(wrgerg)) {
            arg1 = Arrays.asList("reloaddungeon",
                    "addspawner", "setwait", "setduration", "redefine", "wand", "select", "reload", "create", "help");
        }
        List<String> arg2S = new ArrayList<>();
        for(MobType type : MobType.values()) {
            if(!arg2S.contains(type.name()))
                arg2S.add(type.name());
        }
        if(args.length == 1)
            for(String s : arg1)
                if(s.toLowerCase().startsWith(args[args.length+-1].toLowerCase())) f.add(s);
        if(args.length == 2 && (args[0].equalsIgnoreCase("select")||args[0].equalsIgnoreCase("delete")))
            for(Dungeon d : DungeonManager.getDungeons())
                if(d.getName().toLowerCase().startsWith(args[args.length+-1].toLowerCase())) f.add(d.getName());
        if(args.length == 2 && args[0].equalsIgnoreCase("addspawner"))
            for(String a : arg2S)
                if(a.toLowerCase().startsWith(args[args.length+-1].toLowerCase())) f.add(a);
        return f;
    }
    
    @EventHandler
    void onDoubleClick(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if(p.getInventory().getItemInMainHand() != null &&
           !p.getInventory().getItemInMainHand().getType().name().contains("AIR")) {
            ItemStack s = p.getInventory().getItemInMainHand();
            if(s.isSimilar(item)) {
                if(!p.hasPermission("bendingunbroken.dungeon.wand.showBarriers")&&!p.hasPermission("bendingunbroken.dungeon.*")&&!p.hasPermission("bendingunbroken.dungeon.wand.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.wand.showBarriers");
                    return;
                }
                if(e.getAction().name().contains("RIGHT")) {
                    e.setCancelled(true);
                    if(!right1.contains(p.getName()))
                        right1.add(p.getName());
                    (new BukkitRunnable(){
                        @Override
                        public void run() {
                            right1.remove(p.getName());
                        }
                    }).runTaskLater(Load.getInstance(), 5);
                }else if(e.getAction().name().contains("LEFT")) {
                    e.setCancelled(true);
                    if(!left1.contains(p.getName()))
                        left1.add(p.getName());
                    (new BukkitRunnable(){
                        @Override
                        public void run() {
                            left1.remove(p.getName());
                        }
                    }).runTaskLater(Load.getInstance(), 5);
                }
                if(left1.contains(p.getName())&&right1.contains(p.getName())) {
                    if(tasks2.containsKey(p)) {
                        tasks2.get(p).cancel();
                        tasks2.remove(p);
                        if(tasks.containsKey(p)) {
                            tasks.get(p).cancel();
                            tasks.remove(p);
                        }
                        for(Entity ee : entities)
                            ee.remove();
                        entities.clear();
                        if(current.containsKey(p)) {
                            Dungeon d = current.get(p);
                            sendMessage(p, "hideBoarder", d.getName());
                            current.remove(p);
                        }
                        return;
                    }
                    
                    BukkitTask t = (new BukkitRunnable() {
                        public void run() {
                            if(p.getInventory().getItemInMainHand()!=null &&
                               !p.getInventory().getItemInMainHand().getType().name().contains("AIR")) {
                                ItemStack s = p.getInventory().getItemInMainHand();
                                if(s.isSimilar(item)) {
                                    int maxRange = DungeonMain.getInstance().getConfig().getInt("settings.maxRange");
                                    Dungeon dun = DungeonManager.getClosestToPlayer(p, maxRange);
                                    if(dun==null) {
                                        cancel();
                                        sendMessage(p, "Error.cantFindBoarder", maxRange);
                                        return;
                                    }
                                    if(dun != current.get(p) || !current.containsKey(p)) {
                                        doParticles(p, dun);
                                        current.put(p, dun);
                                    }
                                }else {
                                    if(tasks.containsKey(p)) {
                                        tasks.get(p).cancel();
                                        tasks.remove(p);
                                        if(current.containsKey(p))
                                            current.remove(p);
                                        cancel();
                                    }
                                }
                            }else {
                                if(tasks.containsKey(p)) {
                                    tasks.get(p).cancel();
                                    tasks.remove(p);
                                    if(current.containsKey(p))
                                        current.remove(p);
                                    cancel();
                                }
                            }
                        }
                    }).runTaskTimer(Load.getPlugin(Load.class), 5, 20);
                    tasks2.put(p, t);
                    (new BukkitRunnable() {
                        
                        @Override
                        public void run() {
                            Dungeon d = current.get(p);
                            if(d!=null)
                            sendMessage(p, "showBoarder", d.getName());
                        }
                    } ).runTaskLater(Load.getInstance(), 5);
                }
            }
        }
    }
    
    
   
    @EventHandler
    void onHit(EntityDamageEvent e) {
        if(entities.contains(e.getEntity()))e.setCancelled(true);
    }
    
    @EventHandler
    void onClick(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if(users.contains(e.getPlayer().getName()))return;
        else users.add(e.getPlayer().getName());
        (new BukkitRunnable(){@Override public void run() { users.remove(e.getPlayer().getName()); }}).runTaskLater(Load.getPlugin(Load.class), 3);
        if(e.getAction().name().contains("BLOCK")) {
            if(e.getItem()!=null&&e.getItem().isSimilar(item)) {
                if(!p.hasPermission("bendingunbroken.dungeon.wand.setpos")&&!p.hasPermission("bendingunbroken.dungeon.*")&&!p.hasPermission("bendingunbroken.dungeon.wand.*")) {
                    sendMessage(p, "Error.invalidPermission", "bendingunbroken.dungeon.wand.setpos");
                    return;
                }
                if(e.getAction().name().contains("LEFT")) {
                    sendMessage(e.getPlayer(), "setPos1", getLocation(e.getClickedBlock().getLocation()));
                    left.put(e.getPlayer().getUniqueId(), e.getClickedBlock().getLocation());
                }
                if(e.getAction().name().contains("RIGHT")) {
                    sendMessage(e.getPlayer(), "setPos2", getLocation(e.getClickedBlock().getLocation()));
                    right.put(e.getPlayer().getUniqueId(), e.getClickedBlock().getLocation());
                }
                e.setCancelled(true);
            }
        }
    }
    
    boolean isEven(int i) {
        if(i < 0) {
            i = Integer.parseInt((""+i).substring(1));
        }
        if((i % 2) == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    void doEntities(Dungeon d) {
        for(Entity e : entities) {
            DungeonMain.getInstance().scoreboard.getTeam("shulker").removeEntry(e.getUniqueId().toString());
            e.remove();
        }
        entities.clear();
        (new BukkitRunnable() {
            int index = 0;
            int amtInterval = 5;
            @Override
            public void run() {
                try {
                    for (int i = 0; i < amtInterval; i++) {
                        Block b = d.getBox().get(index);
                        loadEntity(b);
                        index++;
                    }
                }catch(Exception e) {
                    cancel();
                }
            }
        }).runTaskTimer(Load.getPlugin(Load.class), 0, 10);
    }
    
    void loadEntity(Block b) {
        Location l = b.getLocation();
        World world = b.getWorld();
        Location l2 = l.clone().add(0.5, 0.5, 0.5);
        if(isEven(l.getBlockX())&&isEven(l.getBlockZ()))
            spawnEntity(world, l2);
    }
    
    void spawnEntity(World w, Location l) {
        (new BukkitRunnable() {
            @Override
            public void run() {
                Slime s = (Slime) w.spawnEntity(l, EntityType.SLIME);
                entities.add(s);
                s.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 9999, 1, true, false));
                s.setGravity(false);
                s.setAI(false);
                s.setInvulnerable(true);
                s.setGlowing(true);
                s.setSize(1);
                DungeonMain.getInstance().scoreboard.getTeam("shulker").addEntry(s.getUniqueId().toString());
            }
        }).runTaskLater(Load.getPlugin(Load.class), i);
        i++;
    }
    
    void doParticles(Player p, Dungeon d) {
        if(tasks.containsKey(p))
            tasks.get(p).cancel();
        BukkitTask task = (new BukkitRunnable() {
            public void run() {
                for(Block b : d.getBox()) {
                    Location l = b.getLocation();
                    if(l.getBlock().getType().isSolid())continue;
                    if(!p.isOnline()) {
                        tasks.remove(p);
                        cancel();
                        break;
                    }
                    if(p.isOnline() && l.distance(p.getLocation()) > 65) continue;
                    World world = b.getWorld();
                    Location l2 = l.clone().add(0.5, 0.5, 0.5);
                    if(d.isCorner(l)) {
                        Dungeon.CornerType t = d.getCorner(l);
                        if(t == Dungeon.CornerType.Y) {
                            for (double a = -0.5; a < 0.5; a += 0.2) {
                                sendParticle(world, l2.clone().add(0, a, 0));
                            }
                        }
                        if(t == Dungeon.CornerType.X_MAX_Z_MAX) {
                            for (double a = 0; a < 0.5; a += 0.2) {
                                sendParticle(world, l2.clone().add(-a, 0, 0));
                                sendParticle(world, l2.clone().add(0, 0, -a));
                            }
                        }
                        if(t == Dungeon.CornerType.X_MAX_Z_MIN) {
                            for (double a = 0; a < 0.5; a += 0.2) {
                                sendParticle(world, l2.clone().add(-a, 0, 0));
                                sendParticle(world, l2.clone().add(0, 0, a));
                            }
                        }
                        if(t == Dungeon.CornerType.X_MIN_Z_MAX) {
                            for (double a = 0; a < 0.5; a += 0.2) {
                                sendParticle(world, l2.clone().add(a, 0, 0));
                                sendParticle(world, l2.clone().add(0, 0, -a));
                            }
                        }
                        if(t == Dungeon.CornerType.X_MIN_Z_MIN) {
                            for (double a = 0; a < 0.5; a += 0.2) {
                                sendParticle(world, l2.clone().add(a, 0, 0));
                                sendParticle(world, l2.clone().add(0, 0, a));
                            }
                        }
                        if(d.isTop(l)) {
                            for (double a = -0.5; a < 0; a += 0.2) {
                                sendParticle(world, l2.clone().add(0, a, 0));
                            }
                        }
                        if(d.isBottom(l)) {
                            for (double a = 0.5; a > 0; a -= 0.2) {
                                sendParticle(world, l2.clone().add(0, a, 0));
                            }
                        }
                        continue;
                    }
                    
                    if(d.getDirection(l2) == Dungeon.Direction.Z) {
                        for (double a = -0.5; a < 1; a += 0.2) {
                            sendParticle(world, l2.clone().add(a, 0, 0));
                        }
                    }
                    if(d.getDirection(l2) == Dungeon.Direction.X) {
                        for (double a = -0.5; a < 1; a += 0.2) {
                            sendParticle(world, l2.clone().add(0, 0, a));
                        }
                    }
                }
            }
        }).runTaskTimer(Load.getPlugin(Load.class), 0, 7);
        tasks.put(p, task);
    }
    
    void sendParticle(World w, Location l) {
        try{
            float red = 0;
            float green = 255;
            float blue = 0;
            w.spawnParticle(Particle.REDSTONE, l.getX(), l.getY(), l.getZ(), 0, 0.001, 1, 0, 1);
        }catch (Exception e) {
            try{
                Particle.DustOptions options = new Particle.DustOptions(Color.fromRGB(0, 255, 0), 1);
                w.spawnParticle(Particle.REDSTONE, l, 2, 0, 0, 0, 1, options);
            }catch (Exception e1) {
            
            }
        }
    }
    
    String getLocation(Location l) {
        return "X: " + l.getBlockX() + " Y: " + l.getBlockY() + " Z: " + l.getBlockZ();
    }
    
    
    
}
