package me.perryplaysmc.dungeon.dungeon;

import me.perryplaysmc.dungeon.DungeonMain;
import me.perryplaysmc.dungeon.util.Config;
import me.perryplaysmc.dungeon.util.StringUtils;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/6/19-2023
 * <p>
 *
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Dungeon {
    
    private DungeonMain main = DungeonMain.getInstance();
    private Timer timer;
    private String name;
    private Config cfg;
    private Location max;
    private Location min;
    private List<ItemStack> possibleLoot;
    private List<Chest> lootChests;
    private List<Spawner> spawners;
    private List<Block> outSide, box;
    private List<Player> players;
    
    public Dungeon(Config cfg) {
        this.cfg = cfg;
        this.name = cfg.getLocalName();
        this.min = cfg.getLocation("settings.Min");
        this.max = cfg.getLocation("settings.Max");
        lootChests = new ArrayList<>();
        possibleLoot = new ArrayList<>();
        spawners = new ArrayList<>();
        outSide = new ArrayList<>();
        box = new ArrayList<>();
        players = new ArrayList<>();
        findLocations();
        loadChests();
        loadLoot();
        loadSpawners();
        fillChests();
        loadBlocks();
        DungeonManager.addDungeon(this);
    }
    
    public Dungeon(String name, Location max, Location min) {
        this.name = name;
        cfg = new Config("plugins/BendingUnbroken/Dungeons",name+".yml");
        this.max = max;
        this.min = min;
        findLocations();
        cfg.set("settings.Max", max);
        cfg.set("settings.Min", min);
        cfg.setIfNotSet("settings.Time.duration", DungeonMain.getInstance().getConfig().getInt("DungeonDefaults.Duration"));
        cfg.setIfNotSet("settings.Time.wait", DungeonMain.getInstance().getConfig().getInt("DungeonDefaults.Wait"));
        this.timer = new Timer(this);
        lootChests = new ArrayList<>();
        possibleLoot = new ArrayList<>();
        spawners = new ArrayList<>();
        outSide = new ArrayList<>();
        box = new ArrayList<>();
        players = new ArrayList<>();
        loadChests();
        loadLoot();
        loadSpawners();
        fillChests();
        loadBlocks();
        DungeonManager.addDungeon(this);
    }
    
    public void redefine(Location min, Location max) {
        this.min = min;
        this.max = max;
        findLocations();
        outSide.clear();
        box.clear();
        loadBlocks();
    }
    
    private void findLocations() {
        if(max==null||min==null) {
            try {
                throw new InvalidLocationException("Please set both locations.");
            } catch (InvalidLocationException e) {
                e.printStackTrace();
            }
        }
        if(max.getWorld().getName()!=min.getWorld().getName()) {
            try {
                throw new InvalidLocationException("Cannot create points between two different worlds.");
            } catch (InvalidLocationException e) {
                e.printStackTrace();
            }
            return;
        }
        int rXMin = (int)Math.min(min.getBlockX(), max.getBlockX());
        int rXMax = (int)Math.max(min.getBlockX(), max.getBlockX());
        int rYMin = (int)Math.min(min.getBlockY(), max.getBlockY());
        int rYMax = (int)Math.max(min.getBlockY(), max.getBlockY());
        int rZMin = (int)Math.min(min.getBlockZ(), max.getBlockZ());
        int rZMax = (int)Math.max(min.getBlockZ(), max.getBlockZ());
        max = new Location(max.getWorld(), rXMax, rYMax, rZMax);
        min = new Location(min.getWorld(), rXMin, rYMin, rZMin);
    }
    
    public void addItemStack(ItemStack stack) {
        cfg.set("Loot.loot", null);
        possibleLoot.add(stack);
        int i = 0;
        for(ItemStack s : possibleLoot) {
            i++;
            cfg.set("Loot.loot.Loot-" + i, s);
        }
    }
    
    public void addChest(Chest chest) {
        cfg.set("Loot.chests", null);
        lootChests.add(chest);
        int i = 0;
        for(Chest s : lootChests) {
            i++;
            cfg.set("Loot.chests.Chest-" + i, s.getLocation());
        }
    }
    
    public void reloadChests() {
        loadChests();
        loadLoot();
        fillChests();
    }
    
    private void loadBlocks() {
        outSide = new ArrayList<>();
        int rXMin = (int)Math.min(min.getBlockX(), max.getBlockX());
        int rXMax = (int)Math.max(min.getBlockX(), max.getBlockX());
        int rYMin = (int)Math.min(min.getBlockY(), max.getBlockY());
        int rYMax = (int)Math.max(min.getBlockY(), max.getBlockY());
        int rZMin = (int)Math.min(min.getBlockZ(), max.getBlockZ());
        int rZMax = (int)Math.max(min.getBlockZ(), max.getBlockZ());
        max = new Location(max.getWorld(), rXMax, rYMax, rZMax);
        min = new Location(min.getWorld(), rXMin, rYMin, rZMin);
        for(int y = min.getBlockY(); y < max.getBlockY()+1; y++) {
            for(int x = min.getBlockX(); x < max.getBlockX()+1; x++) {
                for(int z = min.getBlockZ(); z < max.getBlockZ()+1; z++) {
                    Location loc = new Location(min.getWorld(), x, y, z);
                    
                    if(loc.getBlock().getState() instanceof Chest) {
                        Chest s = (Chest) loc.getBlock().getState();
                        for(ItemStack i : s.getSnapshotInventory().getContents()) {
                            if(i!=null&&!i.getType().name().contains("AIR"))
                                possibleLoot.add(i);
                        }
                        addChest(s);
                    }
                    if(isOutline(loc))
                        if(!box.contains(loc.getBlock()))
                            box.add(loc.getBlock());
                }
            }
        }
    }
    
    public boolean isWithinBoarder(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        return ((x >= xmin && x <= xmax) && (y>=ymin&&y<=ymax) && (z >= zmin && z <= zmax));
    }
    
    public boolean isCorner(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        if(x==xmin&&z==zmin) return true;
        if(x==xmax&&z==zmax) return true;
        if(x==xmin&&z==zmax) return true;
        if(x==xmax&&z==zmin) return true;
        return false;
    }
    
    public boolean isTop(Location l) {
        int ymax = (max.getBlockY());
        int y = l.getBlockY();
        return y==ymax;
    }
    
    public boolean isBottom(Location l) {
        int ymin = (min.getBlockY());
        int y = l.getBlockY();
        return y==ymin;
    }
    
    public CornerType getCorner(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        if(!(y==ymin||y==ymax)&&((x==xmin&&z==zmin)||(x==xmax&&z==zmax)||(x==xmin&&z==zmax)||(x==xmax&&z==zmin))) {
            return CornerType.Y;
        }
        if(x==xmin&&z==zmin) return CornerType.X_MIN_Z_MIN;
        if(x==xmax&&z==zmax) return CornerType.X_MAX_Z_MAX;
        if(x==xmin&&z==zmax) return CornerType.X_MIN_Z_MAX;
        if(x==xmax&&z==zmin) return CornerType.X_MAX_Z_MIN;
        return CornerType.NONE;
    }
    
    public boolean isOutline(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        if(isCorner(l)) return true;
        if(!(y==ymin||y==ymax))return false;
        return (x == xmin || x == xmax) || (z == zmin || z == zmax);
    }
    
    public Direction getDirection(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        if(isCorner(l)) return Direction.NONE;
        if(!(y==ymin||y==ymax))return Direction.NONE;
        if((x == xmin || x == xmax)) return Direction.X;
        if((z == zmin || z == zmax)) return Direction.Z;
        return Direction.NONE;
    }
    
    
    public enum CornerType {
        Y,
        X_MIN_Z_MIN,
        X_MAX_Z_MAX,
        X_MIN_Z_MAX,
        X_MAX_Z_MIN,
        NONE
    }
    public enum Direction {
        X,
        Z,
        NONE
    }
    
    private int findOffSet(int i, int j) {
        int ret = 0;
        if(i > j) {
            int x = 0;
            for(int a = j; a < i; a++) {
                x++;
            }
            return x;
        }
        if(j > i) {
            int x = 0;
            for(int a = i; a < j; a++) {
                x++;
            }
            return x;
        }
        return 0;
    }
    
    public void addSpawner(Spawner spawner) {
        cfg.set("Spawners", null);
        spawners.add(spawner);
        int i = 0;
        for(Spawner s : spawners) {
            i++;
            cfg.set("Spawners.Spawner-" + i + ".location", s.getBlock().getLocation());
            cfg.set("Spawners.Spawner-" + i + ".maxSpawns", s.getMaxSpawns());
            cfg.set("Spawners.Spawner-" + i + ".type", s.getType());
        }
    }
    
    private void loadSpawners() {
        if(cfg.isSet("Spawners"))
            for(String s : cfg.getSection("Spawners").getKeys(false)) {
                Location loc = cfg.getLocation("Spawners."+s+".location");
                Block b = loc.getBlock();
                Spawner spawner = new Spawner(this, b, MobType.valueOf(cfg.getString("Spawners."+s+".type").toUpperCase()), cfg.getInt("Spawners."+s+".maxSpawns"));
                spawners.add(spawner);
            }
    }
    
    private void loadChests() {
        if(cfg.isSet("Loot"))
            if(cfg.isSet("Loot.chests"))
                for(String s : cfg.getSection("Loot.chests").getKeys(false)) {
                    Location loc = cfg.getLocation("Loot.chests."+s);
                    if(loc == null||loc.getWorld()==null||loc.getBlock()==null) {
                        cfg.set("Loot.chests."+s, null);
                        continue;
                    }
                    Block b = loc.getBlock();
                    if(b.getType().name().contains("CHEST"))
                        lootChests.add((Chest) b.getState());
                    else cfg.set("Loot.chests."+s, null);
                }
    }
    
    private void loadLoot() {
        if(cfg.isSet("Loot"))
            if(cfg.isSet("Loot.loot"))
                for(String s : cfg.getSection("Loot.loot").getKeys(false))
                    if(cfg.getItemStack("Loot.loot."+s)!=null)
                        possibleLoot.add(cfg.getItemStack("Loot.loot."+s));
                    else cfg.set("Loot.loot."+s, null);
    }
    
    private void fillChests() {
        for(Chest c : lootChests) {
            int amount = new Random().nextInt(main.getConfig().getInt("DungeonDefaults.maxLootPerChest"));
            if(possibleLoot.size()>0) {
                c.getSnapshotInventory().clear();
                for (int a = 0; a < amount; a++) {
                    int itemIndex = new Random().nextInt(possibleLoot.size());
                    int slot = new Random().nextInt(c.getSnapshotInventory().getSize());
                    while (c.getSnapshotInventory().getItem(slot) != null)
                        slot = new Random().nextInt(c.getSnapshotInventory().getSize());
                    c.getSnapshotInventory().setItem(slot, possibleLoot.get(itemIndex));
                    System.out.println("Added item to chest");
                }
                c.update(true);
            }
        }
    }
    
    
    public List<Block> getBox() {
        return box;
    }
    
    public List<Block> getOutSide() {
        return outSide;
    }
    
    public List<Player> getPlayers() {
        return players;
    }
    
    public void reload() {
        lootChests = new ArrayList<>();
        possibleLoot = new ArrayList<>();
        loadChests();
        loadLoot();
        fillChests();
        reloadChests();
        timer.setPaused(false);
        timer.setReload(false);
    }
    
    
    public List<Spawner> getSpawners() {
        return spawners;
    }
    
    
    public Location getPos1() {
        return max;
    }
    
    public Location getPos2() {
        return min;
    }
    
    
    public String getName() {
        return name;
    }
    
    
    
    public void moveSpawner(Block prev, Block block) {
        for(String s : cfg.getSection("Spawners").getKeys(false))  {
            if(StringUtils.areSimilar(prev.getLocation(), cfg.getLocation("Spawners."+s+".location"))){
                cfg.set("Spawners."+s+".location", block.getLocation());
                break;
            }
        }
    }
    
    public void delete() {
        cfg.deleteConfig();
        DungeonManager.removeDungeon(this);
        timer.cancel();
    }
    
    public Spawner getSpawner(Block block) {
        for(Spawner s : spawners) {
            System.out.println(s.getBlock().getLocation().distance(block.getLocation()));
            if(s.getBlock().getLocation().distance(block.getLocation()) <= 0) {
                return s;
            }
        }
        return null;
    }
    
    public void removeSpawner(Block block) {
        cfg.set("Spawners", null);
        int i = 0;
        spawners.remove(getSpawner(block));
        for(Spawner s : spawners) {
            i++;
            cfg.set("Spawners.Spawner-" + i + ".location", s.getBlock().getLocation());
            cfg.set("Spawners.Spawner-" + i + ".maxSpawns", s.getMaxSpawns());
            cfg.set("Spawners.Spawner-" + i + ".type", s.getType());
        }
    }
    
    public void runSpawners() {
        for(Spawner s : spawners) {
            s.run();
        }
    }
    
    public int getDuration() {
        return cfg.getInt("settings.Time.duration");
    }
    
    public int getWait() {
        return cfg.getInt("settings.Time.wait");
    }
    
    public void setWaitTime(int newTime) {
        cfg.set("settings.Time.wait", newTime);
    }
    
    public void setDurationTime(int newTime) {
        cfg.set("settings.Time.duration", newTime);
    }
    
}
