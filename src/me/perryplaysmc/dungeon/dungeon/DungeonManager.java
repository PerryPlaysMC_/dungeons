package me.perryplaysmc.dungeon.dungeon;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/23/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class DungeonManager {
    private static List<Dungeon> dungeons = new ArrayList<>();
    
    
    public static List<Dungeon> getDungeons() {
        return dungeons;
    }
    
    public static void addDungeon(Dungeon d) {
        if(!dungeons.contains(d))dungeons.add(d);
    }
    
    public static void removeDungeon(Dungeon d) {
        if(dungeons.contains(d))dungeons.remove(d);
    }
    
    public static Dungeon getClosestToPlayer(Player p, double maxRadius) {
        
        double closest = maxRadius;
        Dungeon dun = null;
        for(Dungeon d : getDungeons()) {
            if(d.getPos1().distance(p.getLocation()) < closest) {
                closest = d.getPos1().distance(p.getLocation());
                dun = d;
            }
            if(d.getPos2().distance(p.getLocation()) < closest) {
                closest = d.getPos2().distance(p.getLocation());
                dun = d;
            }
        }
        return dun;
    }
    
    public static Dungeon getDungeon(String name) {
        for(Dungeon d : dungeons) {
            if(d.getName().equalsIgnoreCase(name)) return d;
        }
        return null;
    }
    
    
}
