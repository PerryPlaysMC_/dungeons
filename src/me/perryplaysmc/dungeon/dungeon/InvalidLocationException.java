package me.perryplaysmc.dungeon.dungeon;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/18/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class InvalidLocationException extends Exception {
    
    public InvalidLocationException(String s) {
        super(s);
    }
}