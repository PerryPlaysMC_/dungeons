package me.perryplaysmc.dungeon.dungeon;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/17/19-2023
 **/
public enum MobType {
    
    ZOMBIE, SKELETON, CREEPER, WITHER_SKELETON
    
}
