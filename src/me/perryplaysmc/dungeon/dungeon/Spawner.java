package me.perryplaysmc.dungeon.dungeon;

import me.perryplaysmc.dungeon.dungeon.entities.Creeper;
import me.perryplaysmc.dungeon.dungeon.entities.Skeleton;
import me.perryplaysmc.dungeon.dungeon.entities.WitherSkeleton;
import me.perryplaysmc.dungeon.dungeon.entities.Zombie;
import org.bukkit.Location;
import org.bukkit.block.Block;
import me.perryplaysmc.dungeon.dungeon.entities.*;
import org.bukkit.block.BlockFace;

import java.util.Random;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/17/19-2023
 * <p>
 *
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Spawner {
    
    
    private Block block;
    private MobType type;
    private int maxSpawns, spawns = 0, delay = 10;
    private Dungeon dungeon;
    
    public Spawner(Dungeon dungeon, Block block, MobType type, int maxSpawns) {
        this.dungeon = dungeon;
        this.block = block;
        this.type = type;
        this.maxSpawns = maxSpawns;
        if(maxSpawns < 0) maxSpawns = 20;
    }
    
    
    
    public Block getBlock() {
        return block;
    }
    
    public void setBlock(Block block) {
        Block prev = this.block;
        this.block = block;
        dungeon.moveSpawner(prev, block);
    }
    
    public MobType getType() {
        return type;
    }
    
    public void setType(MobType type) {
        this.type = type;
    }
    
    public int getMaxSpawns() {
        return maxSpawns;
    }
    
    public void setMaxSpawns(int maxSpawns) {
        this.maxSpawns = maxSpawns;
    }
    
    public void run() {
        if(delay <= 0) {
            delay = 10;
            if(spawns > maxSpawns) {
                return;
            }
            int chance = new Random().nextInt(200);
            if(chance < 10) {
                if(maxSpawns > 0) {
                    spawns++;
                }
                Location loc = findLocation();
                if(loc != null)
                    switch (type) {
                        case ZOMBIE:
                            new Zombie(loc);
                            break;
                        case SKELETON:
                            new Skeleton(loc);
                            break;
                        case CREEPER:
                            new Creeper(loc);
                            break;
                        case WITHER_SKELETON:
                            new WitherSkeleton(loc);
                            break;
                    }
            }
        }
        delay--;
    }
    
    private Location findLocation() {
        BlockFace face = BlockFace.UP;
        BlockFace[] faces = new BlockFace[8];
        faces[0] = BlockFace.NORTH;
        faces[1] = BlockFace.SOUTH;
        faces[2] = BlockFace.WEST;
        faces[3] = BlockFace.EAST;
        faces[4] = BlockFace.SOUTH_WEST;
        faces[5] = BlockFace.NORTH_WEST;
        faces[6] = BlockFace.SOUTH_EAST;
        faces[7] = BlockFace.NORTH_EAST;
        int i2 = faces.length;
        int i = new Random().nextInt(i2);
        Block f = block.getRelative(faces[i]);
        while((f.getType().isSolid()||f==null) && i2> 0) {
            i = new Random().nextInt(i2);
            f = block.getRelative(faces[i]);
            i2--;
        }
        return f.getLocation().add(0.5, 0.3, 0.5);
    }
}
