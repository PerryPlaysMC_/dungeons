package me.perryplaysmc.dungeon.dungeon;

import me.perryplaysmc.dungeon.DungeonMain;
import me.perryplaysmc.dungeon.Load;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/6/19-2023
 * <p>
 *
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Timer extends BukkitRunnable {
    
    
    private Dungeon dungeon;
    private boolean paused = false;
    private boolean reload;
    private int time = DungeonMain.getInstance().getConfig().getInt("DungeonDefaults.Duration");
    private int wait = DungeonMain.getInstance().getConfig().getInt("DungeonDefaults.Wait");
    private int refreshTime = DungeonMain.getInstance().getConfig().getInt("DungeonDefaults.Duration");
    private int refreshWait = DungeonMain.getInstance().getConfig().getInt("DungeonDefaults.Wait");
    public Timer(Dungeon dungeon) {
        this.dungeon = dungeon;
        time = dungeon.getDuration();
        wait = dungeon.getWait();
        refreshTime = time;
        refreshWait = wait;
        runTaskTimer(Load.getInstance(), 0, 20);
    }
    
    @Override
    public void run() {
        if(isReload() && isPaused()) {
            dungeon.reload();
            return;
        }
        if(!isPaused()) {
            if(wait > 0) {
                wait--;
                return;
            }
            if(time > 0 && wait <= 0) {
                time--;
                dungeon.runSpawners();
            }
            if(time == 0) {
                wait = refreshWait;
                time = refreshTime;
            }
        }
    }
    
    
    
    
    
    public boolean isReload() {
        return reload;
    }
    
    public void setReload(boolean reload) {
        this.reload = reload;
    }
    
    
    public boolean isPaused() {
        return paused;
    }
    
    public void setPaused(boolean paused) {
        this.paused = paused;
    }
    
    
}
