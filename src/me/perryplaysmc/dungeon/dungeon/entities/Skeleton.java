package me.perryplaysmc.dungeon.dungeon.entities;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Random;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/17/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Skeleton {
    
    public Skeleton(Location loc) {
        org.bukkit.entity.Skeleton skeleton = (org.bukkit.entity.Skeleton)loc.getWorld().spawnEntity(loc, EntityType.SKELETON);
        skeleton.setMaxHealth(new Random().nextInt(20) + 20);
        skeleton.setHealth(skeleton.getMaxHealth());
        skeleton.setCustomName("§cSkeleton");
        skeleton.setCustomNameVisible(true);
        
        
        ItemStack[] stacks = new ItemStack[4];
        Material[] m = new Material[4];
        m[0] = Material.IRON_CHESTPLATE;
        m[1] = Material.DIAMOND_HELMET;
        m[2] = Material.IRON_LEGGINGS;
        m[3] = Material.CHAINMAIL_BOOTS;
        for(int i = 0; i < stacks.length; i++) {
            Material type = m[i];
            ItemStack s = new ItemStack(type, 1);
            ItemMeta im = s.getItemMeta();
            im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, new Random().nextInt(5) + 1, true);
            im.addEnchant(Enchantment.PROTECTION_PROJECTILE, new Random().nextInt(5)+1, true);
            im.addEnchant(Enchantment.PROTECTION_FIRE, new Random().nextInt(5)+1, true);
            s.setItemMeta(im);
            stacks[i]=s;
        }
        Material[] swords = new Material[3];
        swords[0] = Material.GOLDEN_SWORD;
        swords[1] = Material.DIAMOND_SWORD;
        swords[2] = Material.IRON_SWORD;
        
        ItemStack sword = new ItemStack(swords[new Random().nextInt(swords.length)], 1);
        ItemMeta im = sword.getItemMeta();
        im.addEnchant(Enchantment.DAMAGE_ALL, new Random().nextInt(5)+1, true);
        if(new Random().nextInt(10) < 3)
            im.addEnchant(Enchantment.FIRE_ASPECT, new Random().nextInt(2)+1, true);
        if(new Random().nextInt(10) < 3)
            im.addEnchant(Enchantment.SWEEPING_EDGE, new Random().nextInt(3)+1, true);
        sword.setItemMeta(im);
        skeleton.getEquipment().setHelmetDropChance(0);
        skeleton.getEquipment().setBootsDropChance(0);
        skeleton.getEquipment().setChestplateDropChance(0);
        skeleton.getEquipment().setLeggingsDropChance(0);
        
        skeleton.getEquipment().setItemInOffHandDropChance(0);
        
        skeleton.getEquipment().setItemInMainHand(sword);
        skeleton.getEquipment().setArmorContents(stacks);
    }
}
