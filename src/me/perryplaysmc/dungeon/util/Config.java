package me.perryplaysmc.dungeon.util;

import me.perryplaysmc.dungeon.DungeonMain;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/6/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Config {
    
    
    public static final File defaultDirectory = new File("plugins", "BendersUnbroken");
    private File f, dir;
    private String name, copyOf, local;
    private boolean isCopyOf = true;
    private YamlConfiguration cfg;
    private DungeonMain core;
    
    public Config(File dir, File f, YamlConfiguration config) {
        this.dir = dir;
        this.f = f;
        this.cfg = config;
        this.name = f.getName();
        this.copyOf = "";
        this.isCopyOf = false;
        this.core = DungeonMain.getInstance();
        setLocal(f.getName().substring(0, f.getName().lastIndexOf('.')));
        
    }
    
    public Config(DungeonMain core, File dir, String fileToCopy, String name) {
        this.core = core;
        this.name = name;
        if(fileToCopy == "") isCopyOf = false;
        if(dir == null) {
            dir = defaultDirectory;
        }
        if(!dir.isDirectory()) dir.mkdirs();
        
        f = new File(dir, name);
        if(!f.exists()) {
            try {
                if(!isCopyOf) {
                    if(core != null) {
                        if(core.getResource(f.getName()) != null) {
                                FileUtils.copyInputStreamToFile(core.getResource(f.getName()), f);
                        } else {
                            f.createNewFile();
                        }
                    } else {
                        f.createNewFile();
                    }
                } else {
                    if(core != null) {
                        if(core.getResource(fileToCopy) != null) {
                                FileUtils.copyInputStreamToFile(core.getResource(fileToCopy), f);
                        } else {
                            f.createNewFile();
                        }
                    } else {
                        f.createNewFile();
                    }
                }
                System.out.println("Created '" + name + "'");
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(f.getName().endsWith(".yml")) {
            reload();
            setLocal(f.getName().replace(".yml",""));
        }
    }
    
    public Config(String dir, String name) {
        this(DungeonMain.getInstance(), new File(dir), "", name);
    }
    
    public Config(File dir, String fileToCopy, String name) {
        this(DungeonMain.getInstance(), dir, fileToCopy, name);
    }
    
    public Config(File dir, String name) {
        this(DungeonMain.getInstance(), dir, "", name);
    }
    
    public Config(String name) {
        this(DungeonMain.getInstance(), defaultDirectory, "", name);
    }
    
    
    public Config setHeader(String... header) {
        int max = (int) Math.round("                                                                        ".toCharArray().length);
        String h = " ________________________________________________________________________\n" +
                   "/                                                                        \\\n" +
                   "|                                                                        |\n" +
                   "|                                                                        |";
        for(String a : header) {
            h+="\n"+"|"+StringUtils.centerText(max, a)+"|";
        }
        h+="\n"+
           "|                                                                        |\n" +
           "|                                                                        |\n" +
           "|                                                                        |\n" +
           "\\________________________________________________________________________/";
        cfg.options().header(h);
        cfg.options().copyDefaults(true);
        return this;
    }
    
    
    public DungeonMain getDungeonMain() {
        return core;
    }
    
    public Config resetConfig() {
        Config cfg = null;
        try {
            f.delete();
            cfg = new Config(core, dir, copyOf, name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cfg;
    }
    
    public String getName() {
        return name;
    }
    
    public String getLocalName() {
        if(local == null || local.isEmpty()) local = name.replace(".yml", "");
        return local;
    }
    
    public void setLocal(String newLocal) {
        this.local = newLocal;
    }
    
    public File getDirectory()
    {
        return dir;
    }
    
    public File getFile()
    {
        return f;
    }
    
    public YamlConfiguration getConfig()
    {
        return cfg;
    }
    
    public String getString(String path)
    {
        return cfg.getString(path);
    }
    
    public boolean getBoolean(String path)
    {
        return cfg.getBoolean(path);
    }
    
    public Object get(String path) {
        if (cfg.get(path) instanceof String) {
            if(getLocation(path)!=null) {
                return getLocation(getString(path));
            }
            return getString(path);
        }
        return cfg.get(path);
    }
    
    public ConfigurationSection getSection(String path)
    {
        return cfg.getConfigurationSection(path);
    }
    
    public ConfigurationSection createSection(String path)
    {
        return cfg.createSection(path);
    }
    
    public int getInt(String path)
    {
        return cfg.getInt(path);
    }
    
    public double getDouble(String path)
    {
        return cfg.getDouble(path);
    }
    
    public List<String> getStringList(String path)
    {
        return cfg.isSet(path) ? cfg.getStringList(path) : new ArrayList();
    }
    
    public Location getLocation(String path) {
        return (isSet(path) && getString(path).contains("/") && getString(path).split("/").length == 6 ? StringUtils.stringToLocation(getString(path)) : null);
    }
    
    
    public List<Object> getObjectList(String path) {
        return (List<Object>) cfg.getList(path);
    }
    
    public Config set(String path, Object var) {
        if(var instanceof ItemStack) {
            setItemStack(path, (ItemStack)var);
            return this;
        }
        if(var instanceof Enum) {
            cfg.set(path, ((Enum)var).name());
            save();
            return this;
        }
        if ((var instanceof Location)) {
            cfg.set(path, StringUtils.locationToString((Location)var));
            save();
            return this;
        }
        cfg.set(path, var);
        save();
        return this;
    }
    
    public Config setIfNotSet(String path, Object var) {
        if(isSet(path))return this;
        if ((var instanceof Location)) {
            cfg.set(path, StringUtils.locationToString((Location)var));
            save();
            return this;
        }
        cfg.set(path, var);
        save();
        return this;
    }
    
    public List<?> getList(String path) {
        return cfg.getList(path);
    }
    
    private void saveItem(ConfigurationSection section, ItemStack item) {
        if (item == null) return;
        section.set("type", item.getType().name());
        section.set("amount", item.getAmount());
        if (item.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)item.getItemMeta();
            if (im.getOwner() != null)
                section.set("Owner", im.getOwner());
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
            return;
        }
        if (item.hasItemMeta()) {
            ItemMeta im = item.getItemMeta();
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
        }
        section.set("data", item.getDurability());
    }
    
    private ItemStack loadItem(ConfigurationSection section) {
        if(section == null || !isSet(section.getCurrentPath())) {
            return new ItemStack(Material.AIR);
        }
        if(Material.valueOf(section.getString("type")).name().contains("AIR")) {
            cfg.set(section.getCurrentPath(), null);
            return new ItemStack(Material.AIR);
        }
        ItemStack i = new ItemStack(Material.valueOf(section.getString("type")), section.getInt("amount"));
        if (i.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)i.getItemMeta();
            if (section.getString("Owner") != null)
                im.setOwner(section.getString("Owner"));
            if (section.getStringList("enchants") != null) {
                for (String a : section.getStringList("enchants"))
                    im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
            }
            if (section.getStringList("lore") != null) {
                im.setLore(section.getStringList("lore"));
            }
            im.setUnbreakable(section.getBoolean("unbreakable"));
            i.setDurability((short)section.getInt("data"));
            i.setItemMeta(im);
            return i;
        }
        ItemMeta im = i.getItemMeta();
        if (section.getStringList("enchants") != null) {
            for (String a : section.getStringList("enchants"))
                im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
        }
        if (section.getStringList("lore") != null) {
            im.setLore(section.getStringList("lore"));
        }
        im.setUnbreakable(section.getBoolean("unbreakable"));
        i.setDurability((short)section.getInt("data"));
        i.setItemMeta(im);
        return i;
    }
    
    public Config setItemStack(String path, ItemStack item) {
        saveItem(createSection(path), item);
        save();
        return this;
    }
    
    public boolean isSet(String path) {
        return cfg.isSet(path);
    }
    
    public ItemStack getItemStack(String path) {
        return loadItem(getSection(path));
    }
    
    
    public void deleteConfig() {
        String name = getLocalName();
        getFile().delete();
        this.name = "";
    }
    
    public Long getLong(String path) {
        return cfg.getLong(path);
    }
    
    public void reload() {
        cfg = YamlConfiguration.loadConfiguration(f);
    }
    
    public void save() {
        try {
            cfg.save(f);
        } catch (Exception e) {
            System.out.println("Error while saving " + name);
        }
    }
}
