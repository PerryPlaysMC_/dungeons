package me.perryplaysmc.dungeon.util;

import java.io.File;
import java.io.InputStream;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/21/19-2023
 * Package: me.perryplaysmc.dungeon.util
 * Path: me.perryplaysmc.dungeon.util.FileUtils
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class FileUtils {
    
    
    
    public static void copyInputStreamToFile(InputStream source, File destination) {
        try{
            Class.forName("org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils")
                    .getMethod("copyInputStreamToFile", InputStream.class, File.class)
                    .invoke(null, source, destination);
        }catch (Exception e) {
            try{
                Class.forName("org.apache.commons.io.FileUtils")
                        .getMethod("copyInputStreamToFile", InputStream.class, File.class)
                        .invoke(null, source, destination);
            }catch (Exception e1) {
                try{
                    Class.forName("org.apache.commons.io.FileUtils")
                            .getMethod("copyInputStreamToFile", InputStream.class, File.class)
                            .invoke(null, source, destination);
                }catch (Exception e2) {
                
                }
            }
        }
    }
    
}
