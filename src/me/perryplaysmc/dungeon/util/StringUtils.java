package me.perryplaysmc.dungeon.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Project: Dungeons
 * Owner: PerryPlaysMC
 * From: 8/6/19-2023
 * <p>
 *
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class StringUtils {
    
    private static String s = "/";
    
    public static String locationToString(Location l) {
        DecimalFormat f = new DecimalFormat("###.###");
        return l.getWorld().getName() + s + f.format(l.getX()) + s + f.format(l.getY()) + s + f.format(l.getZ()) + s + l.getYaw() + s + l.getPitch();
    }
    
    public static boolean areSimilar(Location l1, Location l2) {
        return l1.getBlockX() == l2.getBlockX() && l1.getBlockY() == l2.getBlockY() && l1.getBlockZ() == l2.getBlockZ();
    }
    
    public static Location stringToLocation(String l) {
        if (!l.contains(s)) return null;
        if(l.split(s).length != 6) return null;
        String[] x = l.split(s);
        return new Location(Bukkit.getWorld(x[0]),
                Double.parseDouble(x[1]),
                Double.parseDouble(x[2]),
                Double.parseDouble(x[3]),
                Float.parseFloat(x[4]),
                Float.parseFloat(x[5]));
    }
    public static String repeat(String toRepeat, int amount) {
        String ret = "";
        for(int i = 0; i < amount; i++) {
            ret+=toRepeat;
        }
        return ret;
    }
    
    public static String translate(String text) {
        for(ChatColor c : ChatColor.values()) {
            if(text.contains("&" + c.getChar()))
                text = text.replace("&" + c.getChar(), "§" + c.getChar());
        }
        return text;
    }
    
    public static List<String> translateList(List<String> texts) {
        List<String> n = new ArrayList<>();
        for(String text : texts) {
            for(ChatColor c : ChatColor.values()) {
                if(text.contains("&" + c.getChar()))
                    text = text.replace("&" + c.getChar(), "§" + c.getChar());
            }
            if(!n.contains(text))
                n.add(text);
        }
        return n;
    }
    
    
    public static String centerText(int maxWidth, String text) {
        int spaces = (int) Math.round((maxWidth - 1.4 * removeColor(text).length()) / 2);
        return repeat(" ", spaces) + text + repeat(" ", spaces);
    }
    
    public static String removeColor(String str) {
        String r = translate(str);
        for(ChatColor c : ChatColor.values()) {
            if(r.contains("§"+c.getChar()))
                r = r.replace("§" + c.getChar(), "");
            else if(r.contains("&"+c.getChar()))
                r = r.replace("&" + c.getChar(), "");
        }
        return r;
    }
    
    
}
